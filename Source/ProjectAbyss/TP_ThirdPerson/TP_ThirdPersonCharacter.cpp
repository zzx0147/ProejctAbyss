// Copyright Epic Games, Inc. All Rights Reserved.

#include "TP_ThirdPersonCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "ProjectAbyss/AnimAttackInterface.h"

//////////////////////////////////////////////////////////////////////////
// ATP_ThirdPersonCharacter

ATP_ThirdPersonCharacter::ATP_ThirdPersonCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	MaxIlluminant = 50;
	Illuminant = 50;
	MaxHP = 100;
	HP = MaxHP;
	STR = 10;
	Speed = 1700.0f;
	DashSpeed = Speed * 3.0f;
	DashTime = 0.66666f;
	DashCoolTime = 4.0f;
	
	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;
	GetCharacterMovement()->MaxSwimSpeed = Speed;
	GetCharacterMovement()->BrakingDecelerationSwimming = Speed * 10.0f;
	GetCharacterMovement()->MaxAcceleration = Speed * 10.0f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 600.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_Andada(
		TEXT("/Game/ProjectAbyss/Characters/Meshes/SK_CH_Andada.SK_CH_Andada"));
	
	
	if (SK_Andada.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SK_Andada.Object);
	}
	GetMesh()->SetCollisionProfileName(TEXT("Player"));

	WeaponCapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("WeaponCollision"));
	WeaponCapsuleComponent->SetupAttachment(GetMesh(),TEXT("SwordSocket"));
	WeaponCapsuleComponent->InitCapsuleSize(22.0f, 110.0f);
	WeaponCapsuleComponent->SetRelativeRotation(FRotator(-90.0f, 0.0f, 0.0f));
	WeaponCapsuleComponent->SetRelativeLocation(FVector(-30.0f, 0.0f, 0.0f));
	WeaponCapsuleComponent->SetCollisionEnabled(ECollisionEnabled::Type::NoCollision);
	WeaponCapsuleComponent->OnComponentBeginOverlap.AddDynamic(this, &ATP_ThirdPersonCharacter::OnWeaponBeginOverlap);

	CurrentState = AndadaState::IDLE;

	DashCollTimeNow = false;
	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void ATP_ThirdPersonCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &ATP_ThirdPersonCharacter::DoAttack);
	PlayerInputComponent->BindAction("Dash", IE_Pressed, this, &ATP_ThirdPersonCharacter::Dash);
	PlayerInputComponent->BindAction("Skill", IE_Pressed, this, &ATP_ThirdPersonCharacter::DoSkill);

	PlayerInputComponent->BindAxis("MoveForward", this, &ATP_ThirdPersonCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATP_ThirdPersonCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ATP_ThirdPersonCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ATP_ThirdPersonCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ATP_ThirdPersonCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ATP_ThirdPersonCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ATP_ThirdPersonCharacter::OnResetVR);
}


void ATP_ThirdPersonCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ATP_ThirdPersonCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void ATP_ThirdPersonCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void ATP_ThirdPersonCharacter::OnMovementModeChanged(EMovementMode PrevMovementMode, uint8 PreviousCustomMode)
{
	ACharacter::OnMovementModeChanged(PrevMovementMode, PreviousCustomMode);

	if (GetCharacterMovement()->MovementMode == EMovementMode::MOVE_Swimming)
	{
		GetCharacterMovement()->RotationRate = FRotator(1080.0f, 1080.0f, 1080.0f);
	}
	else
	{
		GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
		SetActorRotation(FRotator(0.0f, GetActorRotation().Yaw, 0.0f));
	}
}

void ATP_ThirdPersonCharacter::DoAttack()
{
	UAnimInstance* temp = GetMesh()->GetAnimInstance();
	if (temp != nullptr)
	{
		if (GetMesh()->GetAnimInstance()->GetClass()->ImplementsInterface(UAnimAttackInterface::StaticClass()))
		{
			IAnimAttackInterface::Execute_Attack(GetMesh()->GetAnimInstance());
		}
	}
}

void ATP_ThirdPersonCharacter::DoSkill()
{
	/*GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Skill"));*/
	UAnimInstance* temp = GetMesh()->GetAnimInstance();
	if (temp != nullptr && CurrentState == AndadaState::ATTACK)
	{
		if (GetMesh()->GetAnimInstance()->GetClass()->ImplementsInterface(UAnimAttackInterface::StaticClass()))
		{
			IAnimAttackInterface::Execute_Skill(GetMesh()->GetAnimInstance());
		}
	}
}

void ATP_ThirdPersonCharacter::Dash()
{
	if (CurrentState == AndadaState::IDLE && DashCollTimeNow == false)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Dash"));
		CurrentState = AndadaState::DASH;
		GetCharacterMovement()->MaxSwimSpeed = DashSpeed;
		GetCharacterMovement()->MaxAcceleration = Speed * 20.0f;
		FTimerHandle unusedHandle;
		FTimerHandle unusedHandle2;
		GetWorldTimerManager().SetTimer(unusedHandle, this, &ATP_ThirdPersonCharacter::UnDash, DashTime);
		GetWorldTimerManager().SetTimer(unusedHandle2, this, &ATP_ThirdPersonCharacter::DashCoolTimeEnd, DashCoolTime);
		DashCollTimeNow = true;
	}
}

void ATP_ThirdPersonCharacter::UnDash()
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("UnDash"));
	if (CurrentState == AndadaState::DASH)
	{
		CurrentState = AndadaState::IDLE;
	}
	GetCharacterMovement()->MaxSwimSpeed = Speed;
	GetCharacterMovement()->MaxAcceleration = Speed * 10.0f;
}

void ATP_ThirdPersonCharacter::DashCoolTimeEnd()
{
	DashCollTimeNow = false;
}

void ATP_ThirdPersonCharacter::OnWeaponBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!DamagedActors.Contains(OtherActor))
	{
		DamagedActors.Add(OtherActor);
		FDamageEvent damageEvent;
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("ApplyDamage"));
		OtherActor->TakeDamage(STR,damageEvent,GetController(),this);
	}
}

void ATP_ThirdPersonCharacter::WeaponDamageOn()
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("WeaponCollision set PlayerWeapon"));
	WeaponCapsuleComponent->SetCollisionProfileName(TEXT("PlayerWeapon"),true);
}

void ATP_ThirdPersonCharacter::WeaponDamageOff()
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("WeaponCollision set NoCollision"));
	WeaponCapsuleComponent->SetCollisionEnabled(ECollisionEnabled::Type::NoCollision);
	DamagedActors.Empty();	
}

void ATP_ThirdPersonCharacter::WeaponDamageReset()
{
	DamagedActors.Empty();
}

int ATP_ThirdPersonCharacter::GetLightCoreCount()
{
	return Illuminant;
}

void ATP_ThirdPersonCharacter::SetLightCoreCount(int cnt)
{
	Illuminant = cnt;
	IPlayerUMGInterface::Execute_SetIlluminantUI(PlayerUMG.GetObject(), cnt);
}

int ATP_ThirdPersonCharacter::GetMaxHP()
{
	return MaxHP;
}

void ATP_ThirdPersonCharacter::SetCurrentState(TEnumAsByte<AndadaState> newState)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("SetCurrentState"));
	CurrentState = newState;
}

TEnumAsByte<AndadaState> ATP_ThirdPersonCharacter::GetCurrentState() const
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("GetCurrentState"));
	return CurrentState;
}

void ATP_ThirdPersonCharacter::OnConstruction(const FTransform& Transform)
{
	GetCharacterMovement()->MaxSwimSpeed = Speed;
	GetCharacterMovement()->BrakingDecelerationSwimming = Speed * 10.0f;
	GetCharacterMovement()->MaxAcceleration = Speed * 10.0f;
}

void ATP_ThirdPersonCharacter::BeginPlay()
{
	Super::BeginPlay();
	GetCharacterMovement()->MaxSwimSpeed = Speed;
	GetCharacterMovement()->BrakingDecelerationSwimming = Speed * 10.0f;
	GetCharacterMovement()->MaxAcceleration = Speed * 10.0f;
}

void ATP_ThirdPersonCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ATP_ThirdPersonCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ATP_ThirdPersonCharacter::MoveForward(float Value)
{

	if ((Controller != NULL) && (Value != 0.0f) && CurrentState != AndadaState::DASH && CurrentState != AndadaState::DASHATTACK && CurrentState != AndadaState::DEAD)
	{
		if (GetCharacterMovement()->MovementMode == EMovementMode::MOVE_Swimming)
		{
			FVector temp = UKismetMathLibrary::GetForwardVector(FollowCamera->GetComponentRotation());
			lastDirF = temp;
			lastValueF = Value;
			UE_LOG(LogTemp, Log, TEXT("DefaultForward,%s,%f"), *(temp.ToString()), Value);
			AddMovementInput(temp, Value);
		}
		else
		{
			// find out which way is forward
			const FRotator Rotation = Controller->GetControlRotation();
			const FRotator YawRotation(0, Rotation.Yaw, 0);

			// get forward vector
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
			lastDirF = Direction;
			lastValueF = Value;
			AddMovementInput(Direction, Value);
		}
	}
	else if (CurrentState == AndadaState::DASH || CurrentState == AndadaState::DASHATTACK)
	{
		if(lastValueF != 0.0f)
		{
			AddMovementInput(lastDirF, lastValueF);
		}
		else if(lastValueR == 0.0f)
		{
			FVector CharacterForward = UKismetMathLibrary::GetForwardVector(GetRootComponent()->GetComponentRotation());
			UE_LOG(LogTemp, Log, TEXT("DashForward,%s,%f"), *(lastDirF.ToString()), lastValueF);
			AddMovementInput(CharacterForward, 1.0f);
		}
	}
	else
	{
		lastValueF = 0.0f;
	}
}

void ATP_ThirdPersonCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f) && CurrentState != AndadaState::DASH && CurrentState != AndadaState::DASHATTACK && CurrentState != AndadaState::DEAD)
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		lastDirR = Direction;
		lastValueR = Value;
		UE_LOG(LogTemp, Log, TEXT("DefaultRight,%s,%f"), *(Direction.ToString()), Value);
		AddMovementInput(Direction, Value);
	}
	else if (CurrentState == AndadaState::DASH || CurrentState == AndadaState::DASHATTACK)
	{
		UE_LOG(LogTemp, Log, TEXT("DashRight,%s,%f"), *(lastDirR.ToString()), lastValueR);
		if (lastValueR != 0.0f)
		{
			AddMovementInput(lastDirR, lastValueR);
		}
	}
	else
	{
		lastValueR = 0.0f;
	}
}

float ATP_ThirdPersonCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	//if (!ShouldTakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser))
	//{
	//	return 0.f;
	//}

	//// do not modify damage parameters after this
	//const float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	//// respond to the damage
	//if (ActualDamage != 0.f)
	//{
	//	if (EventInstigator && EventInstigator != Controller)
	//	{
	//		LastHitBy = EventInstigator;
	//	}
	//}

	//return ActualDamage;

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Andada get Damage!"));
	HP -= Damage;

	IPlayerUMGInterface::Execute_SetHPUI(PlayerUMG.GetObject(),HP);

	if (HP <= 0.0f)
	{
		SetCurrentState(AndadaState::DEAD);
		IPlayerUMGInterface::Execute_SetDeadUI(PlayerUMG.GetObject());
	}

	return Damage;
}