// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ProjectAbyss/AnimAttackInterface.h"
#include "ProjectAbyss/PlayerUMGInterface.h"
#include "ProjectAbyss/AndadaState.h"

#include "TP_ThirdPersonCharacter.generated.h"


UCLASS(config=Game)
class ATP_ThirdPersonCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCapsuleComponent* WeaponCapsuleComponent;

public:
	ATP_ThirdPersonCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	UPROPERTY(BlueprintGetter = GetCurrentState, BlueprintSetter = SetCurrentState)
	TEnumAsByte<AndadaState> CurrentState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVar")
	FRotator MoveDirection;

	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "Status")
	float MaxHP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status")
	float HP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status")
	float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status")
	float DashSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status")
	float DashTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status")
	float DashCoolTime;

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "Status")
	int32 STR;

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "MyVar")
	int32 MaxIlluminant;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVar")
	int32 Illuminant;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVar")
	TScriptInterface<IPlayerUMGInterface> PlayerUMG;

public:
	FVector lastDirF = FVector::ZeroVector;
	float lastValueF = 0.0f;

	FVector lastDirR = FVector::ZeroVector;
	float lastValueR = 0.0f;

private:
	UPROPERTY()
	UAnimInstance* MyAnimInstance;

	IAnimAttackInterface* AttackAnimInterface;

	TArray<AActor*> DamagedActors;

	bool DashCollTimeNow;
	
protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	virtual void OnMovementModeChanged(EMovementMode PrevMovementMode, uint8 PreviousCustomMode = 0) override;

	void DoAttack();

	void DoSkill();

	void Dash();

	void UnDash();

	void DashCoolTimeEnd();

	UFUNCTION()
	void OnWeaponBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION(BlueprintCallable)
	void WeaponDamageOn();

	UFUNCTION(BlueprintCallable)
	void WeaponDamageOff();

	UFUNCTION(BlueprintCallable)
	void WeaponDamageReset();

	UFUNCTION(BlueprintCallable)
	int GetLightCoreCount();

	UFUNCTION(BlueprintCallable)
	void SetLightCoreCount(int cnt);
	
	UFUNCTION(BlueprintCallable)
	int GetMaxHP();

	UFUNCTION(BlueprintSetter, meta = (BlueprintThreadSafe))
	void SetCurrentState(TEnumAsByte<AndadaState> newState);

	UFUNCTION(BlueprintGetter, meta = (BlueprintThreadSafe))
	TEnumAsByte<AndadaState> GetCurrentState() const;

	virtual void OnConstruction(const FTransform& Transform) override;

	virtual void BeginPlay() override;

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
};

