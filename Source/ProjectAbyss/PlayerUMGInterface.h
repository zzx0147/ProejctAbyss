// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "PlayerUMGInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UPlayerUMGInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class PROJECTABYSS_API IPlayerUMGInterface
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "UI")
	void SetHPUI(float hp);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "UI")
	void SetIlluminantUI(int32 illuminantCnt);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "UI")
	void SetAttackUI(int32 attackCount);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "UI")
	void SetPause();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "UI")
	void SetDeadUI();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "UI")
	void SetBossHPUI(float percent);


	// Add interface functions to this class. This is the class that will be inherited to implement this interface.

};
