// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
UENUM(BlueprintType)
enum AndadaState
{
	IDLE = 0 UMETA(DisplayName = "IDLE"),
	MOVE = 1 UMETA(DisplayName = "MOVE"),
	ATTACK = 2 UMETA(DisplayName = "ATTACK"),
	DASH = 3 UMETA(DisplayName = "DASH"),
	DASHATTACK = 4 UMETA(DisplayName = "DASHATTACK"),
	DEAD = 5 UMETA(DisplayName = "DEAD")
};
